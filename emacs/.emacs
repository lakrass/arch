(require 'package)
(require 'use-package)
(setq use-package-always-ensure t)

(package-initialize)

;; MELPA
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

;; SMOOTH SCROLLING
(setq scroll-margin 5
  scroll-step 1
  scroll-conservatively 10000
  scroll-preserve-screen-position 1)

;; BASIC FEATURES
(setq inhibit-startup-message t)
(set-frame-font "iosevka 16")
(add-hook 'after-init-hook 'global-company-mode)
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)
(ido-mode)
(setq backup-directory-alist '(("." . "~/.emacs_saves")))
(global-display-line-numbers-mode 1)

;; Threat y as yes, n as no
(fset 'yes-or-no-p 'y-or-n-p)


;; PACKAGES
(use-package yasnippet
  :config (yas-global-mode))

(use-package which-key
  :config (which-key-mode))

(use-package company)

(use-package flycheck
  :config (global-flycheck-mode))

(use-package hydra)

(use-package projectile
  :config (projectile-mode))
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

(use-package lsp-treemacs)

(use-package lsp-java
  :config (add-hook 'java-mode-hook 'lsp))

(use-package lsp-mode)

;; JAVA
(add-hook 'java-mode-hook  (lambda ()
			    (setq c-basic-offset 4
				  tab-width 4
				  indent-tabs-mode nil)))

;; JVM args passed to JDT server
(setq lsp-java-vmargs '("-XX:+UseParallelGC" "-XX:GCTimeRatio=4" "-XX:AdaptiveSizePolicyWeight=90" "-Dsun.zip.disableMemoryMapping=true" "-Xmx2G" "-Xms100m"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(gruber-darker))
 '(custom-safe-themes
   '("e27c9668d7eddf75373fa6b07475ae2d6892185f07ebed037eedf783318761d7" default))
 '(package-selected-packages
   '(treemacs lsp-mode lsp-java flycheck hydra use-package which-key magit java-snippets yasnippet company gruber-darker-theme)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
