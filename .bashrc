#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#(cat ~/.cache/wal/sequences &)
export EDITOR=vim
alias ls='ls --color=auto'
PS1="\[\033[01;38;5;142m\]\W\e[1;31m\]\$(git branch 2> /dev/null | sed -e'/^[^*]/d' -e 's/* \(.*\)/(\1)/')\[$(tput sgr0)\] "
#PS1='\[\033[01;38;5;142m\] \W\e[0m\] '
#PS1='\[\033[01;38;5;160m\] \W\e[0m\] ' for the ROOT
# 38;5 - foreground \\ 48;5 - background
HISTSIZE=3000
HISTFILESIZE=10000
export HISTCONTROL=erasedups
alias Flash='sudo mount /dev/sdb1 /home/lakrass/mnt/flash'
alias uFlash='sudo umount /dev/sdb1'
alias mkdir='mkdir -p'
alias feh='feh --draw-filename --auto-zoom --draw-exif'
alias Lari='mpv https://www.youtube.com/playlist?list=PLaq8FyJQEH7BkN8Bv4h12XytazwsiovRp --shuffle'
alias xonotic='xonotic-sdl'
set -o vi
alias lab='cd /home/lakrass/git/'
alias twatch='python3 ~/.scripts/watch.py'
