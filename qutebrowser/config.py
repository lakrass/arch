config.load_autoconfig()
config.bind('<Ctrl-f>', 'hint links spawn mpv {hint-url}')
config.bind('<Alt-f>', 'spawn mpv {url}')
c.downloads.location.prompt = False
c.downloads.location.directory = "/home/lakrass/down/temp/"
c.url.default_page = "https://searx.tuxcloud.net"
c.url.start_pages = "https://searx.tuxcloud.net"
c.url.searchengines = {'DEFAULT':'https://searx.tuxcloud.net/?q={}'}
c.completion.web_history.max_items = 0
c.colors.webpage.preferred_color_scheme = "dark"
